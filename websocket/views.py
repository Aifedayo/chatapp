from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from . models import Connection, ChatMessage
import json
import boto3
  
###########################################################################################
#              T E S T  V I E W  T O  V E R I F Y  O U R   A P I  S E T  U P              #
###########################################################################################
@csrf_exempt
def test(request):
    return JsonResponse({'message' : 'Hello Daud'}, status = 200)

###########################################################################################
#                C R E A T I N G  A  C U S T O M  H E L P E R  F U N C T I O N            #
###########################################################################################
def _parse_body(body):
    body_unicode = body.decode('utf-8')
    # utf-8 will make it readable by json
    return json.loads(str(body_unicode))


###########################################################################################
#            C O N N E C T  V I E W  T O  S A V E  T H E  C O N N E C T I O N  I D        #
###########################################################################################
@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    ConnectionID = Connection(connection_id=connection_id)
    ConnectionID.save()
    print('message : connection successful')
    return JsonResponse({'message' : 'connect successfully'}, status = 200)


###########################################################################################
#    D I S C O N N E C T  V I E W  T O  D E L E T E  T H E   C O N N E C T I O N  I D     #
###########################################################################################
@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connectionID = Connection.objects.get(connection_id=connection_id)
    connectionID.delete()
    print('message : disconnection successful')
    return JsonResponse({'message' : 'disconnect successfully'}, status = 200)


###########################################################################################
#                     A CUSTOM HELPER FUNCTION FOR MY API GATEWAY ACCESS                  #
###########################################################################################  
@csrf_exempt
def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client(
                            'apigatewaymanagementapi', 
                            endpoint_url= "https://terzi305yg.execute-api.us-east-2.amazonaws.com/test/", 
                            region_name = "us-east-2", 
                            aws_secret_access_key = "Sj47TeYxOlEj0BOTIMrzGnzJEJBTGQAltCp3wuW0", 
                            aws_access_key_id = "AKIAI32BJON2LG2TTZPA",
                            )
    
    ConnectionId=connection_id
    Data = json.dumps(data).encode('utf-8')
    response = gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))
    return response
    

###########################################################################################
#             A  F U N C T I O N  T O C R E A T E  A  C H A T  M E S S A G E              #
###########################################################################################
@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)

    chat_message = ChatMessage.objects.create(
                            username = body['body']["username"], 
                            message = body['body']["message"],
                            timestamp = body['body']["timestamp"]
    ).save()

    connections = Connection.objects.all()
    data = {'message':[body]}
    
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
        print(connection)
    return JsonResponse({'message' : 'message sent successfully'}, status = 200)


###########################################################################################
#     F U N C T I O N  T O  G E T  A L L  R E C E N T  M E S S A G E S  I N  O R D E R    #
###########################################################################################  
@csrf_exempt
def get_recent_messages(request):
    body = _parse_body(request.body)
    
    all_messages = []
    messages = ChatMessage.objects.all()
    for message in messages:
        all_messages.append({'username':message.username, "message":message.message, "timestamp":message.timestamp})
    connections = Connection.objects.all()
    data = {'messages':all_messages}
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
        print(connection)
    return JsonResponse({'message' : 'message sent successfully'}, status = 200)


